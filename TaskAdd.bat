@echo off
setlocal
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
if '%errorlevel%' NEQ '0' (
    echo 관리 권한을 요청 ...
    goto UACPrompt
) else ( goto gotAdmin )
:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"
    "%temp%\getadmin.vbs"
    rem del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
pushd "%CD%"
    CD /D "%~dp0"

set /p __NAME=디바이스 이름:
set /p __HOST=서버 주소:
set __LOG=%~dp0Logs
set __PATH=%~dp0o_dust_device.exe '%__NAME%' '%__HOST%' '%__LOG%'
schtasks /Create /RU "SYSTEM" /rl highest /tn o_dust_device /tr "%__PATH%" /sc onstart
if ERRORLEVEL 0 (
    echo 등록 성공
    echo 작업 실행
    schtasks /Run /TN o_dust_device
    if ERRORLEVEL 0 ( echo 실행 성공) else ( echo 실행 실패 )
) else (
  echo 등록 실패
)
pause