@echo off
setlocal
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
if '%errorlevel%' NEQ '0' (
    echo 관리 권한을 요청 ...
    goto UACPrompt
) else ( goto gotAdmin )
:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"
    "%temp%\getadmin.vbs"
    rem del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
pushd "%CD%"
    CD /D "%~dp0"

echo 실행 중지
schtasks /End /TN o_dust_device
if ERRORLEVEL 0 (
    echo 예약 작업 삭제
    schtasks /Delete /TN o_dust_device
    if ERRORLEVEL 0 ( echo 예약 작업 삭제 성공) else ( echo 예약 작업 제거 실패)
) else (
    echo 종료 실패
)
pause