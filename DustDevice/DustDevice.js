"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var os_1 = __importDefault(require("os"));
var Log_1 = __importDefault(require("./Log"));
var Dust_1 = __importDefault(require("./Dust"));
var DustDevice = /** @class */ (function (_super) {
    __extends(DustDevice, _super);
    function DustDevice(path, name) {
        var _this = _super.call(this, path) || this;
        _this._name = "DustDevice";
        if (name) {
            _this._name = name;
        }
        _this.parser = Dust_1.default.parser;
        return _this;
    }
    Object.defineProperty(DustDevice.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    DustDevice.prototype.saveDust = function (dust) {
        _super.prototype.save.call(this, dust.toString() + os_1.default.EOL);
    };
    return DustDevice;
}(Log_1.default));
exports.default = DustDevice;
//# sourceMappingURL=DustDevice.js.map