export default class Dust {
    get pm10(): number {
        return this._pm10;
    }

    set pm10(value: number) {
        this._pm10 = value;
    }
    get pm2_5(): number {
        return this._pm2_5;
    }

    set pm2_5(value: number) {
        this._pm2_5 = value;
    }
    get time(): number {
        return this._time;
    }
    set time(value: number) {
        this._time = value;
    }
    private _pm2_5: number;
    private _pm10: number;
    private _time: number;
    constructor(pm2_5?: number, pm10?: number, time?: number) {
        this._pm2_5 = pm2_5 ? pm2_5 : 0;
        this._pm10 = pm10 ? pm10 : 0;
        this._time = time ? time : Date.now();
    }
    toString() {
        return `${this.pm2_5},${this.pm10},${this.time}`;
    }
    toJSON() {
        return {pm2_5: this.pm2_5, pm10: this.pm10, time: this.time};
    }
    static parser(value: string): any {
        const data = value.split(",");
        return { pm2_5: data[0], pm10: data[1], time: data[2] };
    }
}