import path from "path";
import fs from "fs";
import os from "os";
import {OEventTarget} from "./OEventTarget";


export default class Log extends OEventTarget {
    set parser(value: (value: string) => object) {
        this._parser = value;
    }
    private logsPath: string = "";
    private path: string = "";
    private isInit: boolean = false;

    private _parser: (value: string) => any = (value: string) => value;

    constructor(p: string) {
        super();
        this.logsPath = path.normalize(p);
        this.init();
    }

    init() {
        this.mkdirLogs().then(() => {
            this.setPath();
        });
    }

    private mkdirLogs(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const p = this.logsPath;
            fs.access(p, fs.constants.F_OK, (err) => {
                // console.log(`${path.basename(p)} 폴더 ${err ? "존재하지 않습니다." : "존재."}`);
                if (err) {
                    console.log(`${p} 폴더가 존재하지 않습니다.`);
                    console.log(`${p} 생성`);
                    fs.mkdir(p, err => {
                        if (err) {
                            reject(err);
                        } else {
                            console.log(`${p} 폴더가 준비 완료되었습니다.`);
                            resolve(true);
                        }
                    });
                    return;
                }
                console.log(`${p} 폴더가 준비 완료되었습니다.`);
                resolve(true);
            });
        });
    }
    private setPath() {
        const p = this.logsPath;
        const filenames = fs.readdirSync(p);
        const now = Date.now();
        if (filenames.length > 0) {
            const lastFile = filenames[filenames.length - 1];
            const fileTime = parseInt(path.parse(lastFile).name);
            if (this.checkSameDay(fileTime, now)) {
                // 같은 날이면
                // console.log("same day.");
                console.log(`${(new Date(fileTime)).toLocaleDateString()} 일자 저장 재시작합니다.`);
                this.path = path.format({
                    dir: this.logsPath,
                    base: lastFile,
                });
                this.isInit = true;
                return;
            }
        }
        // console.log("another day.");
        console.log(`${(new Date(now)).toLocaleDateString()} 일자 저장 시작합니다.`);
        this.path = path.format({
            dir: this.logsPath,
            name: now.toString(),
            ext: '.csv'
        });
        this.isInit = true;
    }
    private checkSameDay(d1: number, d2: number): boolean {
        const _d1: Date = new Date(d1);
        const _d2: Date = new Date(d2);
        return _d1.getFullYear() === _d2.getFullYear() &&
            _d1.getMonth() === _d2.getMonth() &&
            _d1.getDate() === _d2.getDate();// &&
            // _d1.getHours() === _d2.getHours() &&
            // _d1.getMinutes() === _d2.getMinutes();
    }

    save(data: string) {
        if (!this.isInit) return;
        if (!this.checkSameDay(parseInt(path.basename(this.path, "csv")), Date.now())) {
            this.setPath();
        }
        fs.appendFile(this.path, data, "utf8", (err) => {
            if (err) throw err;
            // console.log(`The "${data}" was appended to file!`);
        });
    }

    load(callback: (err: Error|null, data: {data: any[], residualLine: number}|null) => void, line: number = 50) {
        if (!this.isInit) return;
        const p = path.dirname(this.path);
        const filePaths = fs.readdirSync(p).map(v => path.join(p, v));
        if (filePaths.length !== 0) {
            this.getDataFromTheEnd(filePaths, line, callback, { data: [], residualLine: 0 });
        } else {
            callback(new Error("No log file."), null);
        }
    }


    private getDataFromTheEnd(filePaths: string[], line: number, callback: (err: Error|null, data: {data: any[], residualLine: number}|null) => void, result: {data: any[], residualLine: number}) {
        if (filePaths.length === 0) {
            callback(null, result);
            return;
        }
        fs.readFile(filePaths[filePaths.length - 1], "utf8", (err, data) => {
            let resultTmp = this.getLastLine(data, line);
            resultTmp.data = resultTmp.data.map(this._parser);
            result.data = resultTmp.data.concat(result.data);
            result.residualLine = resultTmp.residualLine;
            if (result.residualLine === 0) {
                callback(err, result);
                return;
            }
            filePaths.pop();
            this.getDataFromTheEnd(filePaths, result.residualLine, callback, result);
        });
    }
    private getLastLine(data: string, line: number): {data: string[], residualLine: number} {
        let arr = data.split(os.EOL);
        let startIndex = (arr.length - line - 1 < 0) ? 0 : arr.length - line - 1;
        let endIndex = arr.length - 1;
        let residualLine = (line  > arr.length - 1) ? line - (arr.length - 1) : 0;
        return { data: arr.slice(startIndex, endIndex), residualLine: residualLine };
    }
}