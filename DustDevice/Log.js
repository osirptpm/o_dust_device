"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var os_1 = __importDefault(require("os"));
var OEventTarget_1 = require("./OEventTarget");
var Log = /** @class */ (function (_super) {
    __extends(Log, _super);
    function Log(p) {
        var _this = _super.call(this) || this;
        _this.logsPath = "";
        _this.path = "";
        _this.isInit = false;
        _this._parser = function (value) { return value; };
        _this.logsPath = path_1.default.normalize(p);
        _this.init();
        return _this;
    }
    Object.defineProperty(Log.prototype, "parser", {
        set: function (value) {
            this._parser = value;
        },
        enumerable: true,
        configurable: true
    });
    Log.prototype.init = function () {
        var _this = this;
        this.mkdirLogs().then(function () {
            _this.setPath();
        });
    };
    Log.prototype.mkdirLogs = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var p = _this.logsPath;
            fs_1.default.access(p, fs_1.default.constants.F_OK, function (err) {
                // console.log(`${path.basename(p)} 폴더 ${err ? "존재하지 않습니다." : "존재."}`);
                if (err) {
                    console.log(p + " \uD3F4\uB354\uAC00 \uC874\uC7AC\uD558\uC9C0 \uC54A\uC2B5\uB2C8\uB2E4.");
                    console.log(p + " \uC0DD\uC131");
                    fs_1.default.mkdir(p, function (err) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            console.log(p + " \uD3F4\uB354\uAC00 \uC900\uBE44 \uC644\uB8CC\uB418\uC5C8\uC2B5\uB2C8\uB2E4.");
                            resolve(true);
                        }
                    });
                    return;
                }
                console.log(p + " \uD3F4\uB354\uAC00 \uC900\uBE44 \uC644\uB8CC\uB418\uC5C8\uC2B5\uB2C8\uB2E4.");
                resolve(true);
            });
        });
    };
    Log.prototype.setPath = function () {
        var p = this.logsPath;
        var filenames = fs_1.default.readdirSync(p);
        var now = Date.now();
        if (filenames.length > 0) {
            var lastFile = filenames[filenames.length - 1];
            var fileTime = parseInt(path_1.default.parse(lastFile).name);
            if (this.checkSameDay(fileTime, now)) {
                // 같은 날이면
                // console.log("same day.");
                console.log((new Date(fileTime)).toLocaleDateString() + " \uC77C\uC790 \uC800\uC7A5 \uC7AC\uC2DC\uC791\uD569\uB2C8\uB2E4.");
                this.path = path_1.default.format({
                    dir: this.logsPath,
                    base: lastFile,
                });
                this.isInit = true;
                return;
            }
        }
        // console.log("another day.");
        console.log((new Date(now)).toLocaleDateString() + " \uC77C\uC790 \uC800\uC7A5 \uC2DC\uC791\uD569\uB2C8\uB2E4.");
        this.path = path_1.default.format({
            dir: this.logsPath,
            name: now.toString(),
            ext: '.csv'
        });
        this.isInit = true;
    };
    Log.prototype.checkSameDay = function (d1, d2) {
        var _d1 = new Date(d1);
        var _d2 = new Date(d2);
        return _d1.getFullYear() === _d2.getFullYear() &&
            _d1.getMonth() === _d2.getMonth() &&
            _d1.getDate() === _d2.getDate(); // &&
        // _d1.getHours() === _d2.getHours() &&
        // _d1.getMinutes() === _d2.getMinutes();
    };
    Log.prototype.save = function (data) {
        if (!this.isInit)
            return;
        if (!this.checkSameDay(parseInt(path_1.default.basename(this.path, "csv")), Date.now())) {
            this.setPath();
        }
        fs_1.default.appendFile(this.path, data, "utf8", function (err) {
            if (err)
                throw err;
            // console.log(`The "${data}" was appended to file!`);
        });
    };
    Log.prototype.load = function (callback, line) {
        if (line === void 0) { line = 50; }
        if (!this.isInit)
            return;
        var p = path_1.default.dirname(this.path);
        var filePaths = fs_1.default.readdirSync(p).map(function (v) { return path_1.default.join(p, v); });
        if (filePaths.length !== 0) {
            this.getDataFromTheEnd(filePaths, line, callback, { data: [], residualLine: 0 });
        }
        else {
            callback(new Error("No log file."), null);
        }
    };
    Log.prototype.getDataFromTheEnd = function (filePaths, line, callback, result) {
        var _this = this;
        if (filePaths.length === 0) {
            callback(null, result);
            return;
        }
        fs_1.default.readFile(filePaths[filePaths.length - 1], "utf8", function (err, data) {
            var resultTmp = _this.getLastLine(data, line);
            resultTmp.data = resultTmp.data.map(_this._parser);
            result.data = resultTmp.data.concat(result.data);
            result.residualLine = resultTmp.residualLine;
            if (result.residualLine === 0) {
                callback(err, result);
                return;
            }
            filePaths.pop();
            _this.getDataFromTheEnd(filePaths, result.residualLine, callback, result);
        });
    };
    Log.prototype.getLastLine = function (data, line) {
        var arr = data.split(os_1.default.EOL);
        var startIndex = (arr.length - line - 1 < 0) ? 0 : arr.length - line - 1;
        var endIndex = arr.length - 1;
        var residualLine = (line > arr.length - 1) ? line - (arr.length - 1) : 0;
        return { data: arr.slice(startIndex, endIndex), residualLine: residualLine };
    };
    return Log;
}(OEventTarget_1.OEventTarget));
exports.default = Log;
//# sourceMappingURL=Log.js.map