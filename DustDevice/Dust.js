"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Dust = /** @class */ (function () {
    function Dust(pm2_5, pm10, time) {
        this._pm2_5 = pm2_5 ? pm2_5 : 0;
        this._pm10 = pm10 ? pm10 : 0;
        this._time = time ? time : Date.now();
    }
    Object.defineProperty(Dust.prototype, "pm10", {
        get: function () {
            return this._pm10;
        },
        set: function (value) {
            this._pm10 = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Dust.prototype, "pm2_5", {
        get: function () {
            return this._pm2_5;
        },
        set: function (value) {
            this._pm2_5 = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Dust.prototype, "time", {
        get: function () {
            return this._time;
        },
        set: function (value) {
            this._time = value;
        },
        enumerable: true,
        configurable: true
    });
    Dust.prototype.toString = function () {
        return this.pm2_5 + "," + this.pm10 + "," + this.time;
    };
    Dust.prototype.toJSON = function () {
        return { pm2_5: this.pm2_5, pm10: this.pm10, time: this.time };
    };
    Dust.parser = function (value) {
        var data = value.split(",");
        return { pm2_5: data[0], pm10: data[1], time: data[2] };
    };
    return Dust;
}());
exports.default = Dust;
//# sourceMappingURL=Dust.js.map