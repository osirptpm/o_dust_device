import SerialPort from "serialport";

import DustDevice from "./DustDevice";
import Dust from "./Dust";
import {OEvent} from "./OEventTarget";

export default class SDS021 extends DustDevice {
    private port: SerialPort|undefined;

    private checkPort(): Promise<string> {
        return SerialPort.list()
            .then(ports => {
                    for (const p of ports) {
                        if (p.manufacturer === "wch.cn" &&
                            p.vendorId === "1A86" &&
                            p.productId === "7523") {
                            const name: string = p.comName;
                            return Promise.resolve(name);
                        }
                    }
                    return Promise.reject(Error("SDS021 센서 포트를 찾을 수 없습니다."));
                }
            );
    }
    start(callback: (data: Dust) => void, port?: string) { //todo 기기별로 다를테니 적용필요
        const start = (_port: string) => {
            const serialPort = new SerialPort(_port, { autoOpen: false });
            serialPort.on("data", (data) => {
                let checksum = Buffer.alloc(1);
                for (let i = 2; i < 8; i++) {
                    checksum[0] += data[i];
                }
                if (data[8] === checksum[0]) {
                    const pm2_5 = (data[3] * 256 + data[2]) / 10;
                    const pm10 = (data[5] * 256 + data[4]) / 10;
                    callback(new Dust(pm2_5, pm10));
                }
                this.port = serialPort;
            });
            serialPort.on("open", () => {
                this.port = serialPort;
                this.dispatchEvent(new OEvent("open", {port: _port}));
            });
            serialPort.on("close", () => {
                this.port = undefined;
                this.reconnect(callback);
                this.dispatchEvent(new OEvent("close", {port: _port}));
            });
            serialPort.open(error => {
                if (error) {
                    this.reconnect(callback);
                }
            });
        };
        if (!port) {
            this.checkPort().then(start)
                .catch(err => {this.reconnect(callback)});
        } else {
            start(port);
        }
    }
    stop() {
        if (!this.port) return;
        this.port.close(console.log);
    }

    reconnect(callback: (data: Dust) => void): void {
        console.log(`포트 재접속 시도 중...`);
        setTimeout(() => {this.start(callback)}, 1000);
    }
}