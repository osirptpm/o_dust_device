"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OEvent = /** @class */ (function () {
    function OEvent(type, detail) {
        this._target = null;
        this._detail = null;
        this._type = type;
        if (detail) {
            this._detail = detail;
        }
    }
    Object.defineProperty(OEvent.prototype, "detail", {
        get: function () {
            return this._detail;
        },
        set: function (value) {
            this._detail = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OEvent.prototype, "target", {
        set: function (value) {
            this._target = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OEvent.prototype, "type", {
        get: function () {
            return this._type;
        },
        set: function (value) {
            this._type = value;
        },
        enumerable: true,
        configurable: true
    });
    return OEvent;
}());
exports.OEvent = OEvent;
// 참고 https://developer.mozilla.org/ko/docs/Web/API/EventTarget
var OEventTarget = /** @class */ (function () {
    function OEventTarget() {
        this.listeners = {};
    }
    OEventTarget.prototype.addEventListener = function (type, callback) {
        if (!(type in this.listeners)) {
            this.listeners[type] = [];
        }
        this.listeners[type].push(callback);
    };
    OEventTarget.prototype.removeEventListener = function (type, callback) {
        if (!(type in this.listeners)) {
            return;
        }
        var stack = this.listeners[type];
        for (var i = 0, l = stack.length; i < l; i++) {
            if (stack[i] === callback) {
                stack.splice(i, 1);
                return this.removeEventListener(type, callback);
            }
        }
    };
    OEventTarget.prototype.dispatchEvent = function (event) {
        if (!(event.type in this.listeners)) {
            return;
        }
        var stack = this.listeners[event.type];
        event.target = this;
        for (var i = 0, l = stack.length; i < l; i++) {
            stack[i].call(this, event);
        }
    };
    ;
    return OEventTarget;
}());
exports.OEventTarget = OEventTarget;
//# sourceMappingURL=OEventTarget.js.map