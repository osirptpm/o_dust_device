class OEvent {
    get detail(): any {
        return this._detail;
    }

    set detail(value: any) {
        this._detail = value;
    }
    set target(value: any) {
        this._target = value;
    }
    set type(value: string) {
        this._type = value;
    }
    get type(): string {
        return this._type;
    }
    private _type: string;
    private _target: any = null;
    private _detail: any = null;

    constructor(type: string, detail?: any) {
        this._type = type;
        if (detail) {
            this._detail = detail;
        }
    }

}
// 참고 https://developer.mozilla.org/ko/docs/Web/API/EventTarget
class OEventTarget {
    listeners: any = {};
    addEventListener(type: string, callback: (event: OEvent) => void): void {
        if (!(type in this.listeners)) {
            this.listeners[type] = [];
        }
        this.listeners[type].push(callback);
    }
    removeEventListener(type: string, callback: (event: OEvent) => void): void {
        if(!(type in this.listeners)) {
            return;
        }
        let stack = this.listeners[type];
        for(let i = 0, l = stack.length; i < l; i++){
            if(stack[i] === callback){
                stack.splice(i, 1);
                return this.removeEventListener(type, callback);
            }
        }
    }
    dispatchEvent(event: OEvent): void {
        if(!(event.type in this.listeners)) {
            return;
        }
        let stack = this.listeners[event.type];
        event.target = this;
        for(let i = 0, l = stack.length; i < l; i++) {
            stack[i].call(this, event);
        }
    };
}

export {OEvent, OEventTarget};