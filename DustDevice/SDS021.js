"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var serialport_1 = __importDefault(require("serialport"));
var DustDevice_1 = __importDefault(require("./DustDevice"));
var Dust_1 = __importDefault(require("./Dust"));
var OEventTarget_1 = require("./OEventTarget");
var SDS021 = /** @class */ (function (_super) {
    __extends(SDS021, _super);
    function SDS021() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SDS021.prototype.checkPort = function () {
        return serialport_1.default.list()
            .then(function (ports) {
            for (var _i = 0, ports_1 = ports; _i < ports_1.length; _i++) {
                var p = ports_1[_i];
                if (p.manufacturer === "wch.cn" &&
                    p.vendorId === "1A86" &&
                    p.productId === "7523") {
                    var name_1 = p.comName;
                    return Promise.resolve(name_1);
                }
            }
            return Promise.reject(Error("SDS021 센서 포트를 찾을 수 없습니다."));
        });
    };
    SDS021.prototype.start = function (callback, port) {
        var _this = this;
        var start = function (_port) {
            var serialPort = new serialport_1.default(_port, { autoOpen: false });
            serialPort.on("data", function (data) {
                var checksum = Buffer.alloc(1);
                for (var i = 2; i < 8; i++) {
                    checksum[0] += data[i];
                }
                if (data[8] === checksum[0]) {
                    var pm2_5 = (data[3] * 256 + data[2]) / 10;
                    var pm10 = (data[5] * 256 + data[4]) / 10;
                    callback(new Dust_1.default(pm2_5, pm10));
                }
                _this.port = serialPort;
            });
            serialPort.on("open", function () {
                _this.port = serialPort;
                _this.dispatchEvent(new OEventTarget_1.OEvent("open", { port: _port }));
            });
            serialPort.on("close", function () {
                _this.port = undefined;
                _this.reconnect(callback);
                _this.dispatchEvent(new OEventTarget_1.OEvent("close", { port: _port }));
            });
            serialPort.open(function (error) {
                if (error) {
                    _this.reconnect(callback);
                }
            });
        };
        if (!port) {
            this.checkPort().then(start)
                .catch(function (err) { _this.reconnect(callback); });
        }
        else {
            start(port);
        }
    };
    SDS021.prototype.stop = function () {
        if (!this.port)
            return;
        this.port.close(console.log);
    };
    SDS021.prototype.reconnect = function (callback) {
        var _this = this;
        console.log("\uD3EC\uD2B8 \uC7AC\uC811\uC18D \uC2DC\uB3C4 \uC911...");
        setTimeout(function () { _this.start(callback); }, 1000);
    };
    return SDS021;
}(DustDevice_1.default));
exports.default = SDS021;
//# sourceMappingURL=SDS021.js.map