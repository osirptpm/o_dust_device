import os from "os";

import SerialPort from "SerialPort";

import iDustDevice from "./iDustDevice";
import Log from "./Log";
import Dust from "./Dust";

export default abstract class DustDevice extends Log implements iDustDevice {
    set name(value: string) {
        this._name = value;
    }
    get name(): string {
        return this._name;
    }
    private _name: string = "DustDevice";
    constructor(path: string, name?: string) {
        super(path);
        if (name) {
            this._name = name;
        }
        this.parser = Dust.parser;
    }

    abstract start(callback: (data: Dust) => void, port?: string): void;
    abstract stop(): void;

    saveDust(dust: Dust): void {
        super.save(dust.toString() + os.EOL);
    }
}