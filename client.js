"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = __importDefault(require("./config"));
var socket_io_client_1 = __importDefault(require("socket.io-client"));
var SDS021_1 = __importDefault(require("./DustDevice/SDS021"));
config_1.default.name = process.argv[2] || config_1.default.name;
config_1.default.url = process.argv[3] || config_1.default.url;
config_1.default.logsPath = process.argv[4] || config_1.default.logsPath;
console.log("설정 - ", config_1.default);
var sds021 = new SDS021_1.default(config_1.default.logsPath, config_1.default.name);
var namespace = "dust";
var socket = socket_io_client_1.default(config_1.default.url + "/" + namespace, {
    autoConnect: false,
    path: "/dust-sensor",
    query: { name: sds021.name, key: config_1.default.socket_secret_key, type: "device" },
    transports: ['websocket']
});
// 센서 이벤트
sds021.addEventListener("open", function (event) {
    console.log("\uD3EC\uD2B8 " + event.detail.port + " \uC5F0\uACB0 \uC131\uACF5\uD588\uC2B5\uB2C8\uB2E4.");
    console.log("\uC18C\uCF13 \uC11C\uBC84 \uC5F0\uACB0 \uC2DC\uB3C4: " + config_1.default.url + "/" + namespace);
    socket.open();
});
sds021.addEventListener("close", function (event) {
    console.log("\uD3EC\uD2B8 " + event.detail.port + " \uC5F0\uACB0\uC774 \uB04A\uC5B4\uC84C\uC2B5\uB2C8\uB2E4.");
    socket.close();
});
// 소켓 이벤트
socket.on("connect", function () {
    console.log("소켓 서버 접속에 성공했습니다.");
});
socket.on("connect_error", function (error) {
    console.warn("접속에 실패했습니다.", error.message);
});
socket.on("connect_timeout", function (timeout) {
    console.warn("연결 시간이 초과되었습니다.");
});
socket.on("reconnect", function (attemptNumber) {
    console.log("\uC7AC\uC811\uC18D\uC5D0 \uC131\uACF5\uD558\uC600\uC2B5\uB2C8\uB2E4.");
});
socket.on("reconnecting", function (attemptNumber) {
    process.stdout.write(attemptNumber + "\uBC88\uC9F8 \uC7AC\uC811\uC18D \uC2DC\uB3C4 \uC911... ");
});
socket.on("error", function (error) {
    console.warn("서버로부터 에러가 발생했습니다.", error);
    socket.close();
    // socket.open();
    setTimeout(function () { socket.open(); }, 1000);
});
socket.on("disconnect", function (reason) {
    console.warn("서버와 접속이 끊어졌습니다.", reason);
});
socket.on("request-data", function (req) {
    sds021.load(function (err, data) {
        // console.log(data);
        socket.emit("response-data", data);
    }, req.line);
});
// 시작
sds021.start(function (dust) {
    sds021.saveDust(dust);
    if (socket.connected) {
        socket.emit("live-data", dust.toJSON());
    }
});
//# sourceMappingURL=client.js.map