import config from "./config";

import io from "socket.io-client";
import Dust from "./DustDevice/Dust";
import SDS021 from "./DustDevice/SDS021";

config.name = process.argv[2] || config.name;
config.url = process.argv[3] || config.url;
config.logsPath = process.argv[4] || config.logsPath;
console.log("설정 - ", config);

const sds021 = new SDS021(config.logsPath, config.name);

const namespace = "dust";
const socket = io(`${config.url}/${namespace}`, {
    autoConnect: false,
    path: "/dust-sensor",
    query: {name: sds021.name, key: config.socket_secret_key, type: "device"},
    transports: ['websocket']
});

// 센서 이벤트
sds021.addEventListener("open", event => {
    console.log(`포트 ${event.detail.port} 연결 성공했습니다.`);
    console.log(`소켓 서버 연결 시도: ${config.url}/${namespace}`);
    socket.open();
});
sds021.addEventListener("close", event => {
    console.log(`포트 ${event.detail.port} 연결이 끊어졌습니다.`);
    socket.close();
});

// 소켓 이벤트
socket.on("connect", () => {
    console.log("소켓 서버 접속에 성공했습니다.");
});

socket.on("connect_error", (error: Error) => {
    console.warn("접속에 실패했습니다.", error.message);
});
socket.on("connect_timeout", (timeout: any) => {
    console.warn("연결 시간이 초과되었습니다.");
});
socket.on("reconnect", (attemptNumber: number) => {
    console.log(`재접속에 성공하였습니다.`);
});
socket.on("reconnecting", (attemptNumber: number) => {
    process.stdout.write(`${attemptNumber}번째 재접속 시도 중... `);
});
socket.on("error", (error: Error) => {
    console.warn("서버로부터 에러가 발생했습니다.", error);
    socket.close();
    // socket.open();
    setTimeout(() => {socket.open()}, 1000);
});
socket.on("disconnect", (reason: any) => {
    console.warn("서버와 접속이 끊어졌습니다.", reason);
});
socket.on("request-data", (req: {line: number}) => {
    sds021.load((err, data) => {
        // console.log(data);
        socket.emit("response-data", data);
    }, req.line);
});

// 시작
sds021.start((dust: Dust) => {
    sds021.saveDust(dust);
    if (socket.connected) {
        socket.emit("live-data", dust.toJSON());
    }
});