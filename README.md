## SDS021 미세먼지 센서(with Serial-USB 어댑터)

pkg 모듈 사용 -> Native 모듈은 포함되지 않아서 직접 exe 파일과 같은 곳에 위치시켜야 함.

*시리얼 통신으로 받은 데이터를 저장하고 소켓 서버(o_dust_server)로 전송.*

### 설치
*실행 위치에서 Logs 폴더 생성됨.*

1. package.json, o_dust_device.exe, TaskAdd.bat, TaskDelete.bat 폴더에 저장.
2. 실행위치에 package.json 로 npm install
3. TaskAdd.bat 실행 - 부팅시 예약 작업 등록 및 실행.
    4. 서버에 표시될 디바이스 이름, 서버 주소, Logs 폴더 생성 위치 입력.

### 제거

1. TaskDelete.bat 실행 - 프로그램 종료 후 예약 작업 제거.
